/*global $ */
/*eslint-env browser*/

$(document).ready(function () {
    'use strict';


	$(".about span").click(function (e){
        e.preventDefault();
        $(".about p").addClass('active');
        $(this).remove();
    });
    $('#navbar-collapse li').click(function (){
       $("ul.collapse").removeClass("show"); 
    });
});
//========================Load=====================//
$(document).ready(function () {
    "use strict";
    $(window).on('load',function () {
        $(".load").fadeOut(100, function () {
            $("body").css("overflow", "auto");
            $(this).fadeOut(100, function () {
                $(this).remove();
            });
        });
    });

});
/* Add active To aide nav
======================== */
$(document).ready(function () {
    "use strict";
    $("#accordionNav li a").each(function () {
            var t = window.location.href.split(/[?#]/)[0];
            this.href == t && ($(this).addClass("active"), $(this).parent().parent().parent().parent().children('a').addClass("active"));
    });
    
});

$(document).ready(function () {
    'use strict';
    $(window).click(function() {
        $(".collapse").removeClass("show");
    });
    
    $('.collapse ').click(function(event){
        event.stopPropagation();
    });
     
    $("nav li a").click(function (e){

           e.preventDefault();
            $("html, body").animate({
                scrollTop: $("#"+ $(this).data("scroll")).offset().top
            }, 1000);
          //Add Class Active
//          $(this).addClass("active").parent().siblings().find("a").removeClass("active");
       });
       
        // Sycn Navbar
   
        $(window).scroll(function (){
            $("section").each(function (){
                if($(window).scrollTop() > ($(this).offset().top - 50)){
                    var sectionID=$(this).attr("id");
                    $("nav li a ").removeClass("active");
                    $("nav li a[data-scroll='"+ sectionID +"']").addClass("active");
                }
            })
        })
    
});
/* owl
============= */
$(document).ready(function(){
  var owl = $('.home-slider-en .owl-carousel');
    owl.owlCarousel({
        loop:true,
        nav:false,
        dots:true,
        margin:10,
        items:1,
        autoplay:true,
        autoplayTimeout:8000,
        smartSpeed:550,
        autoplayHoverPause:true,
        responsiveClass:true,
    });
    var owl2 = $('.home-slider-ar .owl-carousel');
    owl2.owlCarousel({
        loop:true,
        nav:false,
        dots:true,
        margin:10,
        items:1,
        rtl:true,
        autoplay:true,
        autoplayTimeout:8000,
        smartSpeed:550,
        autoplayHoverPause:true,
        responsiveClass:true,
    });
   
    
});
/* Add active To aide nav
======================== */
$(document).ready(function () {
    "use strict";
    $("nav li a").each(function () {
            var t = window.location.href.split(/[?#]/)[0];
            this.href == t && ($(this).addClass("active"), $(this).parent().children('a').addClass("active"));
        })
});


//========================Scroll To Top=====================//
$(document).ready(function (){

    var scrollButton=$("#scroll");

    $(window).scroll(function(){
        if($(this).scrollTop()>=700)
        {
            scrollButton.show();
        }
        else
        {
            scrollButton.hide();
        }
    });
    scrollButton.click(function(){
        $("html,body").animate({scrollTop:0},900);
    });
});

//========================WOW=====================//
// Init WOW.js and get instance
    var wow = new WOW();
    wow.init();
